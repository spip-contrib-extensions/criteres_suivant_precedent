<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-suivant_precedent?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'suivant_precedent_description' => 'Плагін дозволяє знаходити наступний і попередній елемент у циклі. Використовується для побудови навігації по статям у розділі чи за ключовими словами. ',
	'suivant_precedent_nom' => 'Критерії "Наступний" та "Попередній"',
	'suivant_precedent_slogan' => 'Дозволяє використовувати у циклах нові критерії  <code>{suivant}</code> та <code>{precedent}</code>'
);
