<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-suivant_precedent?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'suivant_precedent_description' => 'Questo plugin è un esperimento per trovare, in un dato ciclo, l’elemento successivo o l’elemento precedente. Questo utilizzo è utile soprattutto per creare navigazioni per argomento o tema (parole chiave).',
	'suivant_precedent_nom' => 'Criteri Avanti / Indietro',
	'suivant_precedent_slogan' => 'Aggiunge criteri per ciclo <code>{suivant}</code> e <code>{precedent}</code> (avanti e indietro)'
);
