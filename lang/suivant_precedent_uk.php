<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/suivant_precedent?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alt_lien_next' => 'Наступна',
	'alt_lien_prev' => 'Попередня',

	// N
	'noisette_description' => 'Виводить посилання на попередню та наступну статтю у розділі. ',
	'noisette_label_meme_rubrique' => 'Показувати посилання лише на статті у тому самому розділі',
	'noisette_nom' => 'Посилання  Настпуна / Попередня'
);
