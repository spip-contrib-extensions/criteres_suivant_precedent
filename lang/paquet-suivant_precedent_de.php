<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-suivant_precedent?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'suivant_precedent_description' => 'Dieses Plugin experimentiert mit dem Auffinden von nachfolgenden und vorangehenden Elementen in den Ergebnissen einer Schleife. Diese Anwendungweise ist vor allem bei der Gestaltung von Navigationselementen in Rubriken- und Themen-Listen (Schlagworte) hilfreich.',
	'suivant_precedent_nom' => 'Kriterium Nächstes / Voriges',
	'suivant_precedent_slogan' => 'Fügt den Kriterien einer Schleife <code>{suivant}</code> (nächstes Element) und <code>{precedent}</code> (voriges Element) hinzu. '
);
