<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/suivant_precedent?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alt_lien_next' => 'Suivant',
	'alt_lien_prev' => 'Précédent',

	// N
	'noisette_description' => 'Affiche les liens de navigation vers l’article précédent et l’article suivant',
	'noisette_label_meme_rubrique' => 'Afficher les liens uniquement vers les articles de la même rubrique',
	'noisette_nom' => 'Liens Précédent / Suivant'
);
